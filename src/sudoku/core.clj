(ns sudoku.core
  (:require [clojure.java.io :as io])
  (:import (java.lang Integer)))

(defn read-sudoku [filename]
  (let [resource (io/resource filename)
        reader (io/reader resource)]
    (let [lines (reduce conj [] (line-seq reader))]
      (vec (map vec (for [line lines] (map #(Integer/parseInt (str %)) line)))))))

(defn duplicates?
  "Checks that if a collection contains duplicates"
  [coll]
  (let [s (set coll)]
    (not (= (count s) (count coll)))))

(defn sudoku-entity-correct
  "Checks that a sudoku entity (coll from 1 to 9) has no duplicates"
  [coll]
  (not (duplicates? (filter #(not (= 0 %1)) coll))))

(defn row-correct?
  "Checks if tne row is correct"
  [index sudoku]
  (let [row (nth sudoku index)]
    (sudoku-entity-correct row)))

(defn column-correct?
  "Checks that if a column is correct"
  [index sudoku]
  (let [column (map #(nth % index) sudoku)]
    (sudoku-entity-correct column)))

(defn extract-block-numbers
  "Get the current numbers of a block with the given row-indexes and col-indexes"
  [row-indexes col-indexes sudoku]
  (vec (for [row-index row-indexes
             col-index col-indexes]
         (nth (nth sudoku row-index) col-index))))

(defn block-correct?
  "Check wether a sudoku 3*3 block is correct (ie. has no duplicates). 
  Block index is from 0 to 8, so that 0 is the first 3*3 block (up left-hand corner)
  and 1 is the seconds block (up center). Index 4 is the fourth block (center left)."
  [index sudoku]
  (let [row-indexes (condp = (int (/ index 3))
                      0 [0 1 2]
                      1 [3 4 5]
                      2 [6 7 8])
        col-indexes (condp = (mod index 3)
                      0 [0 1 2]
                      1 [3 4 5]
                      2 [6 7 8])]
    (let [block (extract-block-numbers row-indexes col-indexes sudoku)]
      (sudoku-entity-correct block))))

(defn sudoku-correct?
  "Check that if the sudoku is correct"
  [sudoku]
  (let [bools (flatten (concat (for [i (range 9)]
                     [(row-correct? i sudoku)
                      (column-correct? i sudoku)
                      (block-correct? i sudoku)])))]
    (every? true? bools)))

(defn cell-value
  "Get the current value of the cell"
  [[row-index col-index] sudoku]
  (nth (nth sudoku row-index) col-index))

(defn get-block-numbers
  "Get the numbers of the block by giving a cell in that particular block"
  [row-index col-index sudoku]
  (let [indexes-by-index (fn [index] (cond
                                      (some #(= index %) [0 1 2]) [0 1 2]
                                      (some #(= index %) [3 4 5]) [3 4 5]
                                      (some #(= index %) [6 7 8]) [6 7 8]))
        row-indexes (indexes-by-index row-index)
        col-indexes (indexes-by-index col-index)]
    (extract-block-numbers row-indexes col-indexes sudoku)))
    

(defn possible-numbers-by-index
  "Get all the possible numbers of a cell that is unknown"
  [row-index col-index sudoku]
  (let [number (nth (nth sudoku row-index) col-index)]
    (if (= 0 (nth (nth sudoku row-index) col-index))
      (let [row-numbers (nth sudoku row-index)
            col-numbers (vec (map #(nth % col-index) sudoku))
            block-numbers (get-block-numbers row-index col-index sudoku)]
        (let [existing-numbers (vec (set (filter #(not= 0 %) (concat row-numbers col-numbers block-numbers))))]
          (loop [existing existing-numbers possible (range 1 10) ret []]
            (cond
             (empty? possible) ret
             (nil? (some #(= % (first possible)) existing)) (recur existing (rest possible)
                                                                   (conj ret (first possible)))
             :else (recur existing (rest possible) ret)))))
      [number])))
    

(defn possible-numbers
  "Go through the sudoku board, and eliminate all the 'easy' cells from the board"
  [sudoku]
  (reduce merge (for [row-index (range (count sudoku))
                      col-index (range (count (first sudoku)))]
                  {[row-index col-index]
                   (possible-numbers-by-index row-index col-index sudoku)})))

(defn calculate-new-grid
  "Add the easily calculateable numbers in the sudoku grid"
  [sudoku possible-numbers-map]
  (let [flat-sudoku (for [row-index (range (count sudoku))
                          col-index (range (count (nth sudoku row-index)))]
                      (let [new-number (get possible-numbers-map [row-index col-index])]
                        (if (= (count new-number) 1)
                          (first new-number)
                          (nth (nth sudoku row-index) col-index))))]
    (vec (map vec (partition (count sudoku) flat-sudoku)))))

(defn order-possibilities-map
  "Orders the map gotten from (sudoku.core/possible-numbers sudoku)"
  [possible]
  (let [sorted-possibilities (into (sorted-map-by (fn [key1 key2]
                           (compare [(count (get possible key2)) key2]
                                    [(count (get possible key1)) key1]))) possible)]
    sorted-possibilities))

(defn find-first-cell-with-multiple-possibilities
  "Finds the first cell with more than one possible correct number"
  [possible]
  (let [ordered (reverse (order-possibilities-map possible))
        no-ones (drop-while #(< (count (last %)) 2) ordered)]
    (first no-ones)))

(defn number-to-cell 
  "Returns a new sudoku board with the value set to the given index"
  [[row-index col-index] value sudoku]
  (assoc sudoku row-index (assoc (nth sudoku row-index) col-index value)))

(defn no-empty-cells?
  "Checks that there are no empty cells in the sudoku"
  [sudoku]
  (every? #(not= 0 %) (flatten sudoku)))

(defn solve-sudoku
  "Solve a sudoku that is give in the sudoku.txt file under the resources folder"
  ([] (solve-sudoku "sudoku.txt"))
  ([file-name]
     (defn inner-loop [sudokus stack]
       (let [sudoku (peek sudokus)]
         (clojure.pprint/pprint sudoku)
         (if (sudoku-correct? sudoku)
           (let [possible (possible-numbers sudoku)
                 new-sudoku (calculate-new-grid sudoku possible)]
             (if (= new-sudoku sudoku)
               (if (no-empty-cells? new-sudoku)
                 new-sudoku
                 (let [cell-with-multiples (find-first-cell-with-multiple-possibilities possible)
                       [row-index col-index] (first cell-with-multiples)
                       values (second cell-with-multiples)]
                   (println "[" row-index col-index "] has values:" values)
                   (for [value values]
                     (inner-loop (conj sudokus (number-to-cell
                                           [row-index col-index]
                                           value new-sudoku))
                                 (conj stack cell-with-multiples)))))
               (recur (conj sudokus new-sudoku) stack)))
           nil)))
     (inner-loop [(read-sudoku file-name)] [])))
